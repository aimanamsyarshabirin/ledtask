const int ledPin = 13; // sets the pin number for the LED

// sets the previous time and interval for each section
unsigned long previousMillis1 = 0;
const long interval1 = 13000; //section 1
unsigned long previousMillis2 = 0;
const long interval2 = 21000; //section 2
unsigned long previousMillis3 = 0;
const long interval3 = 70000; //section 3

void setup() {
  pinMode(ledPin, OUTPUT); // sets the LED pin as output
}

void loop() {
  unsigned long currentMillis = millis(); // gets the current time

  // checks if the current time - previous time for section 1 is greater than or equal to the interval for section 1 and the LED is not already on
  if (currentMillis - previousMillis1 >= interval1 && digitalRead(ledPin) == LOW) {
    previousMillis1 = currentMillis; // updates the previous time for section 1
    digitalWrite(ledPin, HIGH); // turns the LED on
    delay(2000); // keeps the LED on for 2 seconds
    digitalWrite(ledPin, LOW); // turns the LED off
  }

  // checks if the current time - previous time for section 2 is greater than or equal to the interval for section 2 and the LED is not already on
  if (currentMillis - previousMillis2 >= interval2 && digitalRead(ledPin) == LOW) {
    previousMillis2 = currentMillis; // updates the previous time for section 2
    digitalWrite(ledPin, HIGH); // turns the LED on
    delay(4000); // keeps the LED on for 4 seconds
  }

  // checks if the current time - previous time for section 3 is greater than or equal to the interval for section 3 and the LED is not already on
  if (currentMillis - previousMillis3 >= interval3 && digitalRead(ledPin) == LOW) {
    previousMillis3 = currentMillis; // updates the previous time for section 3
    digitalWrite(ledPin, HIGH); // turns the LED on
    delay(10000); // keeps the LED on for 10 seconds
    digitalWrite(ledPin, LOW); // turns the LED off
  }

  // checks if there are multiple sections active at the same time and turns off the LED if it is not following the section with the longest duration
  if (currentMillis - previousMillis1 < interval1 && digitalRead(ledPin) == HIGH) {
    digitalWrite(ledPin, LOW);
  }
  if (currentMillis - previousMillis2 < interval2 && digitalRead(ledPin) == HIGH) {
    digitalWrite(ledPin, LOW);
  }
  if (currentMillis - previousMillis3 < interval3 && digitalRead(ledPin) == HIGH) {
    digitalWrite(ledPin, LOW);
  }
}